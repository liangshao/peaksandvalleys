//: Playground - noun: a place where people can play

import UIKit

//tester: modify this line of integers 
let landStretch = [1,1,2,3,4,5,5,6,6,6,4,3,2,2,3,4,5,6,5,4,3,2,4,2,4,4]

func findPeaksAndValleys(_ nums: [Int]) -> Int {
    var castlesCount = 0
    var indexesToRemove = [Int]()
    
    //interate through array to get the indexes of the identical integers that are next to each other
    for i in 0..<nums.count-1 {
        if nums[i] == nums[i+1] {
            indexesToRemove.append(i)
        }
    }
    
    //remove the repeating integers found
    let numbers = nums
        .enumerated()
        .filter { !indexesToRemove.contains($0.offset) }
        .map { $0.element }
    
    //assuming there are over 3 elements in array
    if numbers.count >= 3 {
        //starts checking from second element
        for i in 1..<numbers.count-1 {
            if (numbers[i - 1] < numbers[i] && numbers[i + 1] < numbers[i])||(numbers[i - 1] > numbers[i] && numbers[i + 1] > numbers[i]) {
                castlesCount += 1
            }
        }
    }
    //first element is always a castle, add it to the result
    return 1 + castlesCount
}

print(findPeaksAndValleys(landStretch))
